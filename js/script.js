var divs = document.getElementsByTagName('div');


function showImg(id){
    $imgSrc = $('#'+id).get(0).src;
    $imgCaption = $('#'+id).eq(0).parent().eq(0).attr('value');
    $('#modal-text').text($imgCaption);
    $("#modal-img").attr('src', $imgSrc);

    document.getElementById("modal-background").style.visibility = "visible";
}

function closeModal(){
    document.getElementById("modal-background").style.visibility = "hidden";
}

$(document).ready(function(){
    $('nav a').click(function(){
        $('html, body').stop().animate({
            scrollTop: $( $(this).attr('href') ).offset().top
        }, 600, 'swing');
        return false;
    });

    $(window).scroll(function(){
        var $anchors = $('.anchor');
        var $nav_bts = $('.nav-button');
        var $size = $anchors.length;
        var i;

        if ($(window).scrollTop() > 50){
            $('nav').addClass('nav-small');
        }
        else{
            $('nav').removeClass('nav-small');
        }

        for (i=0; i<$size; i++)
            $nav_bts.eq(i).removeClass('btn-active');
        for (i = $size-1; i>=0; i--){
            if ($anchors.eq(i).offset().top -5 <= $(window).scrollTop())
                break;
        }
        if (i >= 0)
            $nav_bts.eq($size-i-1).addClass('btn-active');
    });

    $('.carousel li').click(function(e){
        showImg(e.target.id);
    });
});


var $caro = $('.carousel');
var $fade_time = 500;
var $caroPhotos = $caro.find('li');

$caroPhotos.fadeOut();

$caroPhotos.first().addClass('active');
$caroPhotos.first().fadeIn($fade_time);

function moveCarousel($dist){
    var $i = $caro.find('li.active').index();
    $caroPhotos.eq($i).removeClass('active');
    $caroPhotos.eq($i).fadeOut($fade_time);
    $i += $dist;
    if ($caroPhotos.length == $i)
        $i = 0;
    else if(-1 == $i)
        $i = $slides.length-1;
    $caroPhotos.eq($i).fadeIn($fade_time);
    $caroPhotos.eq($i).addClass('active');
}



